﻿using FortCode.DbEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode
{
    public class UserService
    {
        private readonly FortCodeContext _context;

        public UserService()
        {
            
        }
        public UserService(FortCodeContext context)
        {
            _context = context;
        }
        public Users Authenticate(string userName, string passWord)
        {
            return _context.Users.Where(u => u.Email.ToLower() == userName.ToLower() && u.Password == passWord).FirstOrDefault();
        }
    }
}
