﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.DbEntities
{
    public class Cities
    {
        [Key]
        public int CitiesId { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
    }
}
