﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.DbEntities;

namespace FortCode.DbEntities
{
    public class FortCodeContext : DbContext
    {
        public FortCodeContext(DbContextOptions<FortCodeContext> options) : base(options)
        {
        }
        public DbSet<Users> Users { get; set; }
        public DbSet<FortCode.DbEntities.Cities> Cities { get; set; }
        public DbSet<FortCode.DbEntities.FavouriteCities> FavouriteCities { get; set; }
        
    }
}
