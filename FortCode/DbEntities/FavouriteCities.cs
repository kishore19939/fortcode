﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.DbEntities
{
    public class FavouriteCities
    {
        [Key]
        public int FavouriteCitiesId { get; set; }

        [ForeignKey("Users")]
        public int UserId { get; set; }

        [ForeignKey("Cities")]
        public int CitiesId { get; set; }

        public virtual Users Users { get; set; }

        public virtual Cities Cities { get; set; }
    }
}
