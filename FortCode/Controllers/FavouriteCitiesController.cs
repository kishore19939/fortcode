﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FortCode.DbEntities;
using Microsoft.AspNetCore.Authorization;

namespace FortCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FavouriteCitiesController : ControllerBase
    {
        private readonly FortCodeContext _context;

        public FavouriteCitiesController(FortCodeContext context)
        {
            _context = context;
        }
      
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FavouriteCities>>> GetFavouriteCities()
        {
            return await _context.FavouriteCities.ToListAsync();
        }
  
        [HttpPost]
        public async Task<ActionResult<FavouriteCities>> PostFavouriteCities(int UserId, int CitiesId)
        {
            var users = await _context.Users.FindAsync(UserId);
            var cities = await _context.Cities.FindAsync(CitiesId);
            FavouriteCities favouriteCities = new FavouriteCities
            {
                Users = users,
                Cities = cities
            };
            _context.FavouriteCities.Add(favouriteCities);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFavouriteCities", new { id = favouriteCities.FavouriteCitiesId }, favouriteCities);
        }

        
        [HttpDelete("{id}")]
        public async Task<ActionResult<FavouriteCities>> DeleteFavouriteCities(int id)
        {
            var favouriteCities = await _context.FavouriteCities.FindAsync(id);
            if (favouriteCities == null)
            {
                return NotFound();
            }

            _context.FavouriteCities.Remove(favouriteCities);
            await _context.SaveChangesAsync();

            return favouriteCities;
        }

        private bool FavouriteCitiesExists(int id)
        {
            return _context.FavouriteCities.Any(e => e.FavouriteCitiesId == id);
        }
    }
}
