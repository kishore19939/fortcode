using FakeItEasy;
using FortCode;
using FortCode.Controllers;
using FortCode.DbEntities;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using Xunit;

namespace FortCodeApiTests
{
    public class UsersTest
    {
       
        [Fact]
        public void TestSignupwithCorrectData()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);

            var controller = new UsersController(_dbContext);

            Users user = new Users
            {
                Name = "abc",
                Email = "abc123@gmail.com",
                Password = "abc@123"
            };
            var userResult = controller.PostUsers(user);
            Assert.NotNull(userResult);
        }
        [Fact]
        public void TestSignupwithEmailMissing()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);

            var controller = new UsersController(_dbContext);

            Users user = new Users
            {
                Name = "abc",
                Password = "abc@123"
            };
            var userResult = controller.PostUsers(user);
            Assert.Null(userResult.Result.Value);
        }

        [Fact]
        public void TestLoginWithCorrectData()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);
            var controller = new UsersController(_dbContext);
            Users user = new Users
            {
                Name = "abc",
                Email = "abc123@gmail.com",
                Password = "abc@123"
            };
            var userResult = controller.PostUsers(user);
            var userService = new UserService(_dbContext);
            var users=userService.Authenticate(user.Email, user.Password);
            Assert.NotNull(users.Name);
        }


        [Fact]
        public void TestLoginWithIncorrectData()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);
            var controller = new UsersController(_dbContext);
            Users user = new Users
            {
                Name = "abc",
                Email = "abc123@gmail.com",
                Password = "abc@123"
            };
            var userResult = controller.PostUsers(user);
            var userService = new UserService(_dbContext);
            var usersResultAuthenticate = userService.Authenticate(user.Name, user.Password);
            Assert.Null(usersResultAuthenticate);
        }

        [Fact]
        public void TestCreateCity()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);
            var controller = new CitiesController(_dbContext);
            Cities cities = new Cities
            {
                CityName = "Hyderabad",
                Country = "India",
            };
            var citiesResult = controller.PostCities(cities);
            Assert.NotNull(citiesResult);
        }

        [Fact]
        public void TestGetCitiesList()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);
            var controller = new CitiesController(_dbContext);
            Cities cities = new Cities
            {
                CityName = "Hyderabad",
                Country = "India",
            };
            var citiesResult = controller.PostCities(cities);
            var cityList = controller.GetCities();
            Assert.NotNull(cityList);
        }


        [Fact]
        public void  TestCreateFavouriteCity()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);
            var controller = new FavouriteCitiesController(_dbContext);
            var userResult = controller.PostFavouriteCities(1, 1);
            Assert.NotNull(userResult);
        }

        [Fact]
        public void TestDeleteFavouriteCity()
        {
            var optionsBuilder = new DbContextOptionsBuilder<FortCodeContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "UsersDb");
            var _dbContext = new FortCodeContext(optionsBuilder.Options);
            var controller = new FavouriteCitiesController(_dbContext);
            var userResult = controller.DeleteFavouriteCities(1);
            Assert.NotNull(userResult);
        }
    }
}
