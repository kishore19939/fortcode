Steps followed to create the project

Cloned the project and followed Code first approach. 

1. Created the Models(Users, Cities, Favourite cities).

2. Created the DbContext class.

3. Added the "appSettings.Json" file to the project as it is missing.

4. Added the Users, Cities, Favourite cities controllers to perform the operations as per the requirement.

5. Added the Basic authentication class for validating the user. You need to give email and password in postman to post and get data.

6. Added the code such that database will be deleted when you close the application.

Added dlls to the main folder instead of Bin folder as there is a problem in detecting the Dlls from Bin folder. Googled about this could't find the exact solution as some said this is a HP devices issue.

In all the projects I have done, manual unit testing was done so not sure about the automated testing for this.



Test Coverage (Unit Testing)
--> Please check "FortCodeApiTests" folder from the project folder.
--> In this, I have used Moq technique for DbContext with inbuild Memory technique.




Steps to Run the Project:

1. Clone the project

2. Change the connection String:

In Environment variables, create a variable with name as "TaskConnection" and value should be the connection String like below.

Data Source=DESKTOP-LMSKK77\SQLEXPRESS;Initial Catalog=Office;Persist Security Info = True;User Id=myLogin;Password=123;Trusted_Connection=True;MultipleActiveResultSets=true;TrustServerCertificate=True;

If this did not work, uncomment the connection string in appSettings.Json and change the data source values. Also in the Startup.cs file uncomment the services.AddDbContext for appsettings.json.


3. Run the project. 

4. Once the project starts, a database and tables should be created in the sql server that you provided. 

5. Now you can call the API's which are in the API.md file.

6. Data in the database stays as long as the project is running. Once the project is stopped, database will be deleted and it will be created again when you start the project.




