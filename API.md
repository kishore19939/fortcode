API's

1. Create User(HttpPost)--https://localhost:44302/api/Users

Json: {
    "Name":"abc3",
    "Email":"abc4@gmail.com",
    "Password": "abc4@123"

}

Below API's require user authirization. In Postman, Click on "Authorization" and Select Type as "Basic Auth" and then enter the email and password with which you have created the user above.



2. GetUsers List(HttpGet)--https://localhost:44302/api/Users 

3. Get User by UserId(HttpGet)---https://localhost:44302/api/Users/UserId

4. Delete User(HttpDelete)--https://localhost:44302/api/Users/UserId


5. Create City(HttpPost)----https://localhost:44302/api/Cities

Json: {
    "CityName":"abc3",
    "Country":"abc4@gmail.com",
}

6. Get Cities List(HttpGet)----https://localhost:44302/api/Cities

7. Delete City(HttpDelete)---https://localhost:44302/api/Users/UserId


Add favourite City to user--

8. PostFavouriteCities(HttpPost)---https://localhost:44302/api/FavouriteCities?UserId=1&CitiesId=1

9. Delete Favourite City(HttpDelete)---https://localhost:44302/api/FavouriteCities/5
